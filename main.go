package main

import (
	"database/sql"
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"

	_ "github.com/go-sql-driver/mysql"

	"github.com/gorilla/mux"
)

// API key dari raja ongkir
var apikey string = "2c378f8828b6922cf6aedc15e95a93e1"

// Book adalah representasi buku dengan beberapa field
type Book struct {
	ID     int    `json:"id" `
	Title  string `json:"title" `
	Author string `json:"author" `
}

// BookHandler menjalankan query dari MariaDB
func BookHandler(w http.ResponseWriter, r *http.Request) {
	// akses database MariaDB dengan menyebutkan user, pass, dan nama db
	db, err := sql.Open("mysql", "go:go@tcp(127.0.0.1:3306)/test")

	// tangani error jika ada
	if err != nil {
		log.Print(err.Error())
	}
	defer db.Close()

	results, err := db.Query("SELECT id, title, author FROM books")
	if err != nil {
		panic(err.Error()) // proper error handling instead of panic in your app
	}

	var books []Book
	for results.Next() {
		var book Book
		err = results.Scan(&book.ID, &book.Title, &book.Author)
		if err != nil {
			panic(err.Error()) // proper error handling instead of panic in your app
		}
		books = append(books, book)
	}

	//bukujson, _ := json.Marshal(books)
	w.Header().Set("Content-Type", "application/json")
	data := make(map[string]interface{})
	data["status"] = "success"
	data["body"] = books
	resp, _ := json.Marshal(data)
	w.Write(resp)
}

// ProvinceHandler memanggil API raja ongkir
func ProvinceHandler(w http.ResponseWriter, r *http.Request) {

	w.Header().Set("Content-Type", "application/json")
	w.Write(getProvinces())
}

func getProvinces() []byte {
	url := "https://api.rajaongkir.com/starter/province"

	req, _ := http.NewRequest("GET", url, nil)

	req.Header.Add("key", apikey)

	res, err := http.DefaultClient.Do(req)

	if err != nil {
		panic(err.Error()) // proper error handling instead of panic in your app
	}

	defer res.Body.Close()
	body, err := ioutil.ReadAll(res.Body)

	if err != nil {
		panic(err.Error()) // proper error handling instead of panic in your app
	}
	return body
}

func main() {
	r := mux.NewRouter()
	r.HandleFunc("/getBooks", BookHandler).Methods("GET")        // handler adalah contoh query dari MySQL
	r.HandleFunc("/getProvince", ProvinceHandler).Methods("GET") // handler adalah contoh query dari API

	// Layankan di port
	log.Fatal(http.ListenAndServe(":8090", r))
}
